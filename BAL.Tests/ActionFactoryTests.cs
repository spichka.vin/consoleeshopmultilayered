﻿using System;
using System.Collections.Generic;
using System.Text;
using NUnit.Framework;
using BAL.Action;

namespace BAL.Tests
{
    [TestFixture]
    class ActionFactoryTests
    {
        [Test]
        public void GetAdminActionReturnsAdminAction()
        {
            ActionFactory actionFactory = new ActionFactory();
            Assert.That(actionFactory.GetAdminAction(), Is.TypeOf<AdminAction>());
        }
        [Test]
        public void GetUserActionReturnsUserAction()
        {
            ActionFactory actionFactory = new ActionFactory();
            Assert.That(actionFactory.GetUserAction(), Is.TypeOf<UserAction>());
        }
        [Test]
        public void GetGuestActionReturnsGuestAction()
        {
            ActionFactory actionFactory = new ActionFactory();
            Assert.That(actionFactory.GetGuestAction(), Is.TypeOf<GuestAction>());
        }
    }
}
