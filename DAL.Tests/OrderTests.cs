﻿using NUnit.Framework;
using System.Linq;
using ConsoleEShopMultilayered;
using DAL.IDao;
using DAL.Dao;
using DB;
using DB.Users;
using System.Collections.Generic;

namespace DAL.Tests
{
    [TestFixture]
    class OrderTests
    {
        [SetUp]
        public void Init()
        {
            DataInitializer initializer = new DataInitializer();
            initializer.UserInitializer();
            initializer.ProductInitialize();
            initializer.OrderInitializer();
        }
        [TearDown]
        public void Declin()
        {
            DataInitializer initializer = new DataInitializer();
            initializer.Decliner();
        }
        [TestCase(2)]
        public void GetAllReturnsRightOrderCount(int expected)
        {
            IOrderDao orderDao = new OrderDao();
            Assert.AreEqual(expected, orderDao.GetAll().Count);
        }
        [TestCase("Alevs", 1)]
        public void GetAllByUserReturnsRightOrderCount(string nickname, int expected)
        {
            IOrderDao orderDao = new OrderDao();
            IUserDao userDao = new UserDao();
            Assert.AreEqual(expected, orderDao.GetAllByUser(userDao.GetUserByNickname(nickname).First()).Count);
        }
        [TestCase("Alevs", OrderStatus.Sent, 0)]
        [TestCase("user", OrderStatus.Sent, 1)]
        public void GetAllByUserAndOrderStatusReturnsRightOrderCount(string nickname, OrderStatus status, int expected)
        {
            IOrderDao orderDao = new OrderDao();
            IUserDao userDao = new UserDao();
            Guest user = userDao.GetUserByNickname(nickname).First();
            bool tmp = OrderStatus.Sent.Equals(status);
            Assert.AreEqual(expected,
                orderDao.GetAllByUserAndOrderStatus(user, status).Count);
        }
        [TestCase(1)]
        public void GetOrderByIdReturnsRightOrderList(int id)
        {
            IOrderDao orderDao = new OrderDao();
            List<Order> orderList = orderDao.GetOrderById(id);
            Assert.AreEqual(id, orderList.First().Id);
        }
        [TestCase(1, OrderStatus.Received)]
        public void SetOrderStatusChangesOrderStatus(int id, OrderStatus status)
        {
            IOrderDao orderDao = new OrderDao();
            Order order = orderDao.GetOrderById(id).First();
            orderDao.SetOrderStatus(order, status);
            Assert.AreEqual(status, orderDao.GetOrderById(id).First().Status);
        }
    }
}
