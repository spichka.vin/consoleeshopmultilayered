﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BAL.Action
{
    public class ActionFactory
    {
        public GuestAction GetGuestAction()
        {
            return new GuestAction();
        }
        public UserAction GetUserAction()
        {
            return new UserAction();
        }
        public AdminAction GetAdminAction()
        {
            return new AdminAction();
        }
    }
}
