﻿using System;
using System.Collections.Generic;
using System.Text;
using DB.Users;
using System.Linq;

namespace DB
{
    public class Order
    {
        public int Id
        {
            get; set;
        }
        public Guest Customer
        {
            get; set;
        }
        public Product Product
        {
            get; set;
        }

        public OrderStatus Status
        {
            get; set;
        }

        public Order(Guest customer, Product product)
        {
            if (Data.orderList.Count == 0)
            {
                this.Id = 1;
            }
            else
            {
                this.Id = Data.orderList.Select(x => x.Id).Max() + 1;
            }
            this.Customer = customer;
            this.Product = product;
            this.Status = OrderStatus.New;
        }

        public override string ToString()
        {
            string str = "\nOrder:\n";
            str +=  "Id: " + this.Id + "\n";
            str += "Customer: " + this.Customer.Nickname + "\n";
            str += "\nProduct:" + this.Product.ToString() + "\n";
            str += "Status: " + this.Status + "\n";
            return str;
        }

    }
}
