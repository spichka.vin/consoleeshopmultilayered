﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace DB.Users
{
    public class User : Guest
    {
        public User(string nickname, string password)
        {
            if (Data.userList.Count == 0)
            {
                this.Id = 1;
            }
            else
            {
                this.Id = Data.userList.Select(x => x.Id).Max() + 1;
            }
            this.Nickname = nickname;
            this.Password = password;
            this.Age = 18;
        }

    }
}
