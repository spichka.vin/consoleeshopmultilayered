﻿using NUnit.Framework;
using DAL;
using DAL.Dao;

namespace DAL.Tests
{
    [TestFixture]
    class FactoryTest
    {
        [Test]
        public void GetUserDaoReturnsUserDao()
        {
            DaoFactory daoFactory = new DaoFactory();
            Assert.That(daoFactory.GetUserDao(), Is.TypeOf<UserDao>());
        }
        [Test]
        public void GetUserDaoReturnsProductDao()
        {
            DaoFactory daoFactory = new DaoFactory();
            Assert.That(daoFactory.GetProductDao(), Is.TypeOf<ProductDao>());
        }
        [Test]
        public void GetUserDaoReturnsOrderDao()
        {
            DaoFactory daoFactory = new DaoFactory();
            Assert.That(daoFactory.GetOrderDao(), Is.TypeOf<OrderDao>());
        }
    }
}
