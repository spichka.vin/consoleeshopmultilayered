﻿using System;
using System.Collections.Generic;
using System.Text;
using DB;
using DB.Users;
using BAL.Action;

namespace UIL.Interraction
{
    public class BaseInterraction
    {
        protected readonly ActionFactory actionFactory = new ActionFactory();
        public void ShowMenu(Guest user)
        {
            if (user.GetType().Equals(typeof(Guest)))
            {
                Console.WriteLine("\nHello Guest, you can do a few things:");
            }
            else
            {
                Console.WriteLine($"\nHello {user.Nickname}, you can do a few things:");
            }
            Console.WriteLine("1. Show all products.");
            Console.WriteLine("2. Search product by its name.");
        }
        protected void CreateOrderMenu()
        {
            Console.WriteLine("3. Create new order.");
        }
        protected void ExitMenu()
        {
            Console.WriteLine("0. Exit");
        }
        public bool BaseRequestCompiler(string command)
        {
            switch (command)
            {
                case "0":
                    return false;
                case "1":
                    Console.WriteLine(actionFactory.GetGuestAction().FindAllProducts());
                    break;
                case "2":
                    FindProductByNameCompiller();
                    break;
                default:
                    Console.WriteLine("\nIncorrect command.");
                    break;
            }
            return true;
        }
        private void FindProductByNameCompiller()
        {
            Console.WriteLine("Input product name:");
            string name = Console.ReadLine();
            try
            {
                Console.WriteLine(actionFactory.GetGuestAction().FindProductByName(name));
            }
            catch(ArgumentNullException ex)
            {
                Console.WriteLine("\n" + ex.Message);
            }         
        }
        protected bool NewOrderCompiler(string inCommand, Guest user, ref Order order)
        {
            if(inCommand != "3")
            {
                return false;
            }
            if (order is null)
            {
                TmpOrderCreateCompiler(user, ref order);
            }
            if (!(order is null))
            {
                OrderCorfimCompiler(ref order);
            }
            return true;
        }
        private void OrderCorfimCompiler(ref Order order)
        {
            Console.WriteLine("\nYour order:");
            Console.WriteLine(order.ToString());
            Console.WriteLine("\nDo you want to confirm your order? \n Yes - 1, No - 2, Later - 3");
            bool flag = true;
            while (flag)
            {
                string command = Console.ReadLine();
                switch (command)
                {
                    case "1":
                        actionFactory.GetUserAction().ConfirmOrderCreating(order);
                        order = null;
                        flag = false;
                        break;
                    case "2":
                        order = null;
                        flag = false;
                        break;
                    case "3":
                        flag = false;
                        break;
                    default:
                        Console.WriteLine("\nIncorrect command");
                        break;
                }
            }
        }
        private void TmpOrderCreateCompiler(Guest user, ref Order order)
        {
            try
            {
                Console.WriteLine(actionFactory.GetGuestAction().FindAllProducts());
                Console.WriteLine("\nEnter product id you want buy:");
                string productId = Console.ReadLine();
                order = actionFactory.GetUserAction().CreateNewOrder(user, productId);
                if (order is null)
                {
                    Console.WriteLine("\nThere is no product with such id.");
                }
                else
                {
                    Console.WriteLine("\nOrder created seccessfuly.");
                }
            }
            catch (ArgumentNullException ex)
            {
                Console.WriteLine("\n" + ex.Message);
            }
            catch (ArgumentException ex)
            {
                Console.WriteLine("\n" + ex.Message);
            }
        }

    }
}
