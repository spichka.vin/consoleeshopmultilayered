﻿using System;
using System.Collections.Generic;
using System.Text;
using DB;
using DB.Users;
using UIL.Interraction;

namespace UIL
{
    public class Menu
    {
        private Guest user;
        private Order tmpOrder;
        private readonly InterractionFactory interractionFactory = new InterractionFactory();
        public Menu()
        {
            user = new Guest();
            tmpOrder = null;
        }

        public void Ranner()
        {
            bool flag = true;
            while (flag)
            {
                if (user.GetType().Equals(typeof(Guest)))
                {
                    interractionFactory.GetGuestInterraction().ShowMenu(user);
                    string command = Console.ReadLine();
                    flag = interractionFactory.GetGuestInterraction().RequestCompiler(command, ref user);
                }
                if (user.GetType().Equals(typeof(User)))
                {
                    interractionFactory.GetUserInterraction().ShowMenu(user);
                    string command = Console.ReadLine();
                    flag = interractionFactory.GetUserInterraction().RequestCompiler(command, ref user, ref tmpOrder);
                }
                if (user.GetType().Equals(typeof(Admin)))
                {
                    interractionFactory.GetAdminInterraction().ShowMenu(user);
                    string command = Console.ReadLine();
                    flag = interractionFactory.GetAdminInterraction().RequestCompiler(command, ref user, ref tmpOrder);
                }
            }
        }
    }
}
