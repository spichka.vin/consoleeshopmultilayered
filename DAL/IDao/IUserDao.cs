﻿using System;
using System.Collections.Generic;
using System.Text;
using DB.Users;

namespace DAL.IDao
{
    public interface IUserDao
    {
        List<Guest> GetAll();
        List<Guest> GetUserByNickname(string nickname);
        List<Guest> GetUserById(int id);
        void Add(Guest user);
        void SetId(Guest user, int id);
        void SetPassword(Guest user, string password);
        void SetAge(Guest user, int age);
    }
}
