﻿using System;
using System.Linq;
using NUnit.Framework;
using ConsoleEShopMultilayered;
using BAL.Action;
using DB.Users;
using DB;

namespace BAL.Tests
{
    [TestFixture]
    class UserActionTests
    {
        [SetUp]
        public void Init()
        {
            DataInitializer initializer = new DataInitializer();
            initializer.UserInitializer();
            initializer.ProductInitialize();
            initializer.OrderInitializer();
        }
        [TearDown]
        public void Declin()
        {
            DataInitializer initializer = new DataInitializer();
            initializer.Decliner();
        }
        [Test]
        public void CreateNewOrderThrowsArgumentNullException()
        {
            Guest user = Data.userList.First();
            UserAction userAction = new UserAction();
            Assert.Throws<ArgumentNullException>(() => userAction.CreateNewOrder(user, ""));
        }
        [Test]
        public void CreateNewOrderThrowsArgumentException()
        {
            Guest user = Data.userList.First();
            UserAction userAction = new UserAction();
            Assert.Throws<ArgumentException>(() => userAction.CreateNewOrder(user, "asd"));
        }
        public void SetOrderStatusThrowsArgumentNullException()
        {
            Guest user = Data.userList.First();
            UserAction userAction = new UserAction();
            Assert.Throws<ArgumentNullException>(() => userAction.SetOrderStatus("", user, OrderStatus.Received));
        }
        [Test]
        public void SetOrderStatusThrowsArgumentException()
        {
            Guest user = Data.userList.First();
            UserAction userAction = new UserAction();
            Assert.Throws<ArgumentException>(() => userAction.SetOrderStatus("asd", user, OrderStatus.Received));
        }
        [Test]
        public void SetUserPasswordThrowsArgumentNullException()
        {
            Guest user = Data.userList.First();
            UserAction userAction = new UserAction();
            Assert.Throws<ArgumentNullException>(() => userAction.SetUserPassword(user, ""));
        }
        public void SetUserAgeThrowsArgumentNullException()
        {
            Guest user = Data.userList.First();
            UserAction userAction = new UserAction();
            Assert.Throws<ArgumentNullException>(() => userAction.SetUserAge(user, ""));
        }
        [Test]
        public void SetUserAgeThrowsArgumentException()
        {
            Guest user = Data.userList.First();
            UserAction userAction = new UserAction();
            Assert.Throws<ArgumentException>(() => userAction.SetUserAge(user, "asd"));
        }
    }
}
