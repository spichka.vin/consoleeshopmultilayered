﻿using System;
using UIL;

namespace ConsoleEShopMultilayered
{
    class Program
    {
        static void Main(string[] args)
        {
            DataInitializer dataInitializer = new DataInitializer();
            Menu menu = new Menu();
            dataInitializer.ProductInitialize();
            dataInitializer.UserInitializer();
            dataInitializer.OrderInitializer();
            menu.Ranner();
        }
    }
}
