﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using DB;
using DB.Users;
using DAL;

namespace ConsoleEShopMultilayered
{
    public class DataInitializer
    {
        private readonly DaoFactory daoFactory = new DaoFactory();
        public void ProductInitialize()
        {
            Product product1 = new Product("Asus ROG", "Laptop", "Gaming laptop", 1500);
            Data.productList.Add(product1);
            Product product2 = new Product("Apple 10", "Smartphone", "Powerfull smartphone with perfect desine", Convert.ToDecimal(999.99));
            Data.productList.Add(product2);
            Product product3 = new Product("Sony PlayStation 5", "Game console", "Next gen console.", Convert.ToDecimal(399.50));
            Data.productList.Add(product3);
        }
        public void UserInitializer()
        {
            Admin admin1 = new Admin("admin", "admin");
            Data.userList.Add(admin1);
            User user1 = new User("user", "user");
            Data.userList.Add(user1);
            User user2 = new User("Alevs", "1111");
            Data.userList.Add(user2);
        }
        public void OrderInitializer()
        {
            Guest user = daoFactory.GetUserDao().GetUserByNickname("user").First();
            Product product = daoFactory.GetProductDao().GetProductByName("Asus ROG").First();
            Order order = new Order(user, product);
            order.Status = OrderStatus.Sent;
            Data.orderList.Add(order);
            Guest user2 = daoFactory.GetUserDao().GetUserByNickname("Alevs").First();
            Product product2 = daoFactory.GetProductDao().GetProductByName("Sony PlayStation 5").First();
            Order order2 = new Order(user2, product2);
            order2.Status = OrderStatus.PaymentReceived;
            Data.orderList.Add(order2);
        }
        public void Decliner()
        {
            Data.orderList.Clear();
            Data.productList.Clear();
            Data.userList.Clear();
        }
    }
}
