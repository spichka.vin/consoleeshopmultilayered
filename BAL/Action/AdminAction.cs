﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
using System.Linq;
using DB;
using DB.Users;
using DAL;

namespace BAL.Action
{
    public class AdminAction
    {
        private readonly DaoFactory daoFactory = new DaoFactory();
        public string GetAllUser()
        {
            List<Guest> userList = daoFactory.GetUserDao().GetAll();
            return CreateStringGuest(userList, "\nThere is no user.");
        }
        public string GetAllOrders()
        {
            List<Order> orderList = daoFactory.GetOrderDao().GetAll();
            return CreateStringOrder(orderList, "\nThere is no order.");
        }
        public string ChangeUserId(string oldId, string newId)
        {
            IdExceptionChecker(oldId);
            Guest oldUser = GetUserById(oldId);
            if (oldUser is null)
            {
                return $"\nThere is no user with id = {oldId}.";
            }
            if (!(GetUserById(newId) is null))
            {
                return $"\nUser with id = {newId} already exist.";
            }
            daoFactory.GetUserDao().SetId(oldUser, Convert.ToInt32(newId));
            Guest newUser = GetUserById(newId);
            daoFactory.GetOrderDao().UpdateUserInAllOrder(oldUser, newUser);
            return "\nUpdated successfuly.";
        }
        public Guest GetUserById(string id)
        {
            IdExceptionChecker(id);
            List<Guest> userList = daoFactory.GetUserDao().GetUserById(Convert.ToInt32(id));
            if (userList.Count == 0)
            {
                return null;
            }
            return userList.First();
        }
        public Product GetProductById(string id)
        {
            IdExceptionChecker(id);
            List<Product> productList = daoFactory.GetProductDao().GetProductById(Convert.ToInt32(id));
            if(productList.Count == 0)
            {
                return null;
            }
            return productList.First();
        }
        public string CreateProduct(string name, string category, string description, string cost)
        {
            if(name.Length == 0)
            {
               throw new ArgumentNullException(nameof(name), "Argument is null.");
            }
            if (category.Length == 0)
            {
                throw new ArgumentNullException(nameof(category), "Argument is null.");
            }
            if (cost.Length == 0)
            {
                throw new ArgumentNullException(nameof(cost), "Argument is null.");
            }
            if (!IsDecimal(cost))
            {
                throw new ArgumentException("Incorrect value format.", nameof(cost));
            }
            List<Product> productList = daoFactory.GetProductDao().GetProductByName(name);
            if(productList.Count != 0)
            {
                return "\nProduct with this name already exist.";
            }
            Product product = new Product(name, category, description, Convert.ToDecimal(cost, new CultureInfo("en-US")));
            daoFactory.GetProductDao().Add(product);
            return "\nProdeuct created successfuly";
        }
        public string ChangeProductId(string oldId, string newId)
        {
            IdExceptionChecker(oldId);
            Product oldProduct = GetProductById(oldId);
            if (oldProduct is null)
            {
                return $"\nThere is no user with id = {oldId}.";
            }
            if (!(GetProductById(newId) is null))
            {
                return $"\nProduct with id = {newId} already exist.";
            }
            daoFactory.GetProductDao().SetId(oldProduct, Convert.ToInt32(newId));
            Product newProduct = GetProductById(newId);
            daoFactory.GetOrderDao().UpdateProductInAllOrder(oldProduct, newProduct);
            return "\nUpdated successfuly.";
        }
        public string ChangeProductCost(string id, string cost)
        {
            IdExceptionChecker(id);
            Product oldProduct = GetProductById(id);
            if (oldProduct is null)
            {
                return $"\nThere is no user with id = {id}.";
            }
            if(cost.Length == 0)
            {
                throw new ArgumentNullException(nameof(cost), "Argument is null.");
            }
            if (!IsDecimal(cost))
            {
                throw new ArgumentException("\nIncorrect value format.", nameof(cost));
            }
            daoFactory.GetProductDao().SetCost(oldProduct, Convert.ToDecimal(cost, new CultureInfo("en-US")));
            Product newProduct = GetProductById(id);
            daoFactory.GetOrderDao().UpdateProductInAllOrder(oldProduct, newProduct);
            return "\nUpdated successfuly.";
        }
        public string ChangeProductDescription(string id, string description)
        {
            IdExceptionChecker(id);
            Product oldProduct = GetProductById(id);
            if (oldProduct is null)
            {
                return $"\nThere is no user with id = {id}.";
            }
            daoFactory.GetProductDao().SetDescription(oldProduct, description);
            Product newProduct = GetProductById(id);
            daoFactory.GetOrderDao().UpdateProductInAllOrder(oldProduct, newProduct);
            return "\nUpdated successfuly.";
        }
        public string SetOrderStatus(string id, OrderStatus status)
        {
            IdExceptionChecker(id);
            List<Order> orderList = daoFactory.GetOrderDao().GetOrderById(Convert.ToInt32(id));
            if (orderList.Count == 0)
            {
                return $"There is no order with id = {id}";
            }
            daoFactory.GetOrderDao().SetOrderStatus(orderList.First(), status);
            return "Status was set successfuly.";
        }
        private string CreateStringGuest(List<Guest> userList, string msg)
        {
            string str = "";
            foreach (Guest user in userList)
            {
                str += user.ToString();
            }
            if (str.Length == 0)
            {
                return msg;
            }
            return str;
        }
        private string CreateStringOrder(List<Order> orderList, string msg)
        {
            string str = "";
            foreach (Order order in orderList)
            {
                str += order.ToString();
            }
            if (str.Length == 0)
            {
                return msg;
            }
            return str;
        }
        private bool IsInt(string str)
        {
            string newStr = new String(str.Where(c => c != '-' && (c < '0' || c > '9')).ToArray());
            if (newStr.Length > 0)
            {
                return false;
            }
            return true;
        }
        private bool IsDecimal(string str)
        {
            string newStr = new String(str.Where(c => c != '-' && (c < '0' || c > '9')).ToArray());
            if (newStr.Length != 1 && newStr != ".")
            {
                return false;
            }
            return true;
        }
        private void IdExceptionChecker(string id)
        {
            if (id.Length == 0)
            {
                throw new ArgumentNullException(nameof(id), "Argument is null.");
            }
            if (!IsInt(id))
            {
                throw new ArgumentException("Value is not a number.", nameof(id));
            }
        }
    }
}
