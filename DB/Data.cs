﻿using System;
using System.Collections.Generic;
using System.Text;
using DB.Users;

namespace DB
{
    public static class Data
    {
        public static List<Guest> userList = new List<Guest>();
        public static List<Product> productList = new List<Product>();
        public static List<Order> orderList = new List<Order>();
    }
}
