﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DB.Users
{
    public class Guest
    {
        public int Id
        {
            get;
            set;
        }
        public string Nickname
        {
            get; 
            protected set;
        }
        public string Password
        {
            get;
            set;
        }
        public int Age
        {
            get; set;
        }
        public Guest()
        {
            Id = 0;
            Nickname = null;
            Password = null;
            Age = 0;
        }

        public override string ToString()
        {
            string str = "\nId: " + this.Id.ToString() + "\n";
            str +=    "Nickname: " + this.Nickname + "\n";
            str += "Password: " + this.Password + "\n";
            str += "Age: " + this.Age + "\n";
            return str;
        }
    }
}
