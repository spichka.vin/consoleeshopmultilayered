﻿using System;
using System.Collections.Generic;
using System.Text;
using DB;
using DB.Users;

namespace UIL.Interraction
{
    public class UserInterraction : BaseInterraction
    {
        public void ShowMenu(Guest user)
        {
            base.ShowMenu(user);
            base.CreateOrderMenu();
            Console.WriteLine("4. Show orders history.");
            Console.WriteLine("5. Change order status.");
            Console.WriteLine("6. Change personal information");
            Console.WriteLine("7. Sign out.");
            ExitMenu();
        }
        public bool RequestCompiler(string command, ref Guest user, ref Order order)
        {
            switch (command)
            {
                case "4":
                    Console.WriteLine(actionFactory.GetUserAction().FindOrderHistory(user));
                    break;
                case "5":
                    ChangeStatusCompiler(user);
                    break;
                case "6":
                    PersonalInfoChangingCompiler(ref user);
                    break;
                case "7":
                    order = null;
                    user = new Guest();
                    break;
                default:
                    if (!base.NewOrderCompiler(command, user, ref order))
                    {
                        return base.BaseRequestCompiler(command);
                    }
                    break;
            }
            return true;
        }
        private void ChangeStatusCompiler(Guest user)
        {
            try
            {
                Console.WriteLine(actionFactory.GetUserAction().GetOrderWithSentPaymentReceivedNewStatus(user));
                Console.WriteLine("\nEnter order id\nExit - 0");
                string orderId = Console.ReadLine();
                Console.WriteLine("\nEnter status you want to set:");
                bool flag = true;
                OrderStatus status = OrderStatus.Received;
                while (orderId != "0" && flag)
                {
                    Console.WriteLine($"\n1. {OrderStatus.Received}.");
                    Console.WriteLine($"2. {OrderStatus.Canceled}.");
                    Console.WriteLine($"0. Exit.");
                    string command = Console.ReadLine();
                    switch (command) 
                    {
                        case "1":
                            flag = false;
                            break;
                        case "2":
                            flag = false;
                            status = OrderStatus.Canceled;
                            break;
                        case "0":
                            orderId = "0";
                            break;
                        default:
                            Console.WriteLine("\nIncorrect command");
                            break;
                    }                      
                }
                if (orderId != "0")
                {
                    Console.WriteLine(actionFactory.GetUserAction().SetOrderStatus(orderId, user, status));
                }
            }
            catch(ArgumentNullException ex)
            {
                Console.WriteLine("\n" + ex.Message);
            }
            catch(ArgumentException ex)
            {
                Console.WriteLine("\n" + ex.Message);
            }
            
        }
        private void PersonalInfoChangingCompiler(ref Guest user)
        {
            Console.WriteLine("\n1. Change password");
            Console.WriteLine("\n2. Change age");
            Console.WriteLine("\n0. Exit");
            string command = Console.ReadLine();
            switch (command)
            {
                case "1":
                    ChangePassword(ref user);
                    break;
                case "2":
                    ChangeAge(ref user);
                    break;
                case "0":
                    break;
                default:
                    Console.WriteLine("\nIncorrect command.");
                    break;
            }
        }
        private void ChangePassword(ref Guest user)
        {
            try
            {
                Console.WriteLine($"\nPassword: {user.Password}.");
                Console.WriteLine("\nEnter new password:");
                string password = Console.ReadLine();
                Console.WriteLine(actionFactory.GetUserAction().SetUserPassword(user, password));
                user = actionFactory.GetGuestAction().LogIn(user.Nickname, password);
            }
            catch (ArgumentNullException ex)
            {
                Console.WriteLine("\n" + ex.Message);
            }
        }
        private void ChangeAge(ref Guest user)
        {
            try
            {
                Console.WriteLine($"\nAge: {user.Age}.");
                Console.WriteLine("\nEnter new age:");
                string age = Console.ReadLine();
                Console.WriteLine(actionFactory.GetUserAction().SetUserAge(user, age));
                user = actionFactory.GetGuestAction().LogIn(user.Nickname, user.Password);
            }
            catch (ArgumentNullException ex)
            {
                Console.WriteLine("\n" + ex.Message);
            }
            catch (ArgumentException ex)
            {
                Console.WriteLine("\n" + ex.Message);
            }
        }

    }
}
