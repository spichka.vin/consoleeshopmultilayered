﻿using System;
using System.Collections.Generic;
using System.Text;
using DB;
using DB.Users;

namespace UIL.Interraction
{
    public class AdminInterraction : BaseInterraction
    {
        public void ShowMenu(Guest user)
        {
            base.ShowMenu(user);
            base.CreateOrderMenu();
            Console.WriteLine("4. Watch and change user's personal information.");
            Console.WriteLine("5. Add new product.");
            Console.WriteLine("6. Change information about product.");
            Console.WriteLine("7. Change order status.");
            Console.WriteLine("8. Sign out.");
            ExitMenu();
        }
        public bool RequestCompiler(string command, ref Guest user, ref Order order)
        {
            switch (command)
            {
                case "4":
                    UptadeUserInfoCompiler(ref user, ref order);
                    break;
                case "5":
                    NewProductCompiler();
                    break;
                case "6":
                    ChangeProductInfoCompiler(ref order);
                    break;
                case "7":
                    ChangeOrderStatusCompiler();
                    break;
                case "8":
                    order = null;
                    user = new Guest();
                    break;
                default:
                    if (!base.NewOrderCompiler(command, user, ref order))
                    {
                        return base.BaseRequestCompiler(command);
                    }
                    break;
            }
            return true;
        }
        private void UptadeUserInfoCompiler(ref Guest user, ref Order order)
        {
            Console.WriteLine(actionFactory.GetAdminAction().GetAllUser());
            Console.WriteLine("\nEnter user id tou want modify.");
            string userId = Console.ReadLine();
            Console.WriteLine("\nModify:");
            Console.WriteLine("1. Id.");
            Console.WriteLine("2. Password.");
            Console.WriteLine("3. Age.");
            Console.WriteLine("Default: Exit.");
            string command = Console.ReadLine();
            switch (command) 
            {
                case "1":
                    UpdateUserIdCompiler(ref user, userId);
                    order = null;
                    break;
                case "2":
                    UpdateUserPasswordCompiler(ref user, userId);
                    order = null;
                    break;
                case "3":
                    UpdateUserAgeCompiler(ref user, userId);
                    order = null;
                    break;
                default:
                    break;
            }
        }          
        private void UpdateUserIdCompiler(ref Guest user ,string oldId)
        {
            Console.WriteLine("\nEnter new id.");
            string newId = Console.ReadLine();
            try
            {
                Console.WriteLine(actionFactory.GetAdminAction().ChangeUserId(oldId, newId));
            }
            catch (ArgumentNullException ex)
            {
                Console.WriteLine(ex.Message);
            }
            catch(ArgumentException ex)
            {
                Console.WriteLine(ex.Message);
            }
            user = actionFactory.GetGuestAction().LogIn(user.Nickname, user.Password);
        }
        private void UpdateUserPasswordCompiler(ref Guest user, string id)
        {
            Console.WriteLine("\nEnter new password.");
            string password = Console.ReadLine();
            try
            {
                Guest userForUpdate = actionFactory.GetAdminAction().GetUserById(id);
                if(userForUpdate is null)
                {
                    Console.WriteLine("\nThere is no user with this id");
                    return;
                }
                Console.WriteLine(actionFactory.GetUserAction().SetUserPassword(userForUpdate, password));
                if (userForUpdate.Nickname.Equals(user.Nickname))
                {
                    user = actionFactory.GetGuestAction().LogIn(userForUpdate.Nickname, userForUpdate.Password);
                }
            }
            catch (ArgumentNullException ex)
            {
                Console.WriteLine(ex.Message);
            }
            catch (ArgumentException ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
        private void UpdateUserAgeCompiler(ref Guest user, string id)
        {
            Console.WriteLine("\nEnter new age.");
            string age = Console.ReadLine();
            try
            {
                Guest userForUpdate = actionFactory.GetAdminAction().GetUserById(id);
                if (userForUpdate is null)
                {
                    Console.WriteLine("\nThere is no user with this id");
                    return;
                }
                Console.WriteLine(actionFactory.GetUserAction().SetUserAge(userForUpdate, age));
                if (userForUpdate.Nickname.Equals(user.Nickname))
                {
                    user = actionFactory.GetGuestAction().LogIn(userForUpdate.Nickname, userForUpdate.Password);
                }
            }
            catch (ArgumentNullException ex)
            {
                Console.WriteLine(ex.Message);
            }
            catch (ArgumentException ex)
            {
                Console.WriteLine(ex.Message);
            }
            user = actionFactory.GetGuestAction().LogIn(user.Nickname, user.Password);
        }
        private void NewProductCompiler()
        {
            Console.WriteLine("\nEnter product name.");
            string name = Console.ReadLine();
            Console.WriteLine("\nEnter product category.");
            string category = Console.ReadLine();
            Console.WriteLine("\nEnter product description.");            
            string description = Console.ReadLine();
            Console.WriteLine("\nEnter product cost.");
            Console.WriteLine("\nInput format: XXX.XX");
            string cost = Console.ReadLine();
            try
            {
                Console.WriteLine(actionFactory.GetAdminAction().CreateProduct(name, category, description, cost));
            }
            catch(ArgumentNullException ex)
            {
                Console.WriteLine(ex.Message);
            }
            catch(ArgumentException ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
        private void ChangeProductInfoCompiler(ref Order order)
        {
            Console.WriteLine(actionFactory.GetGuestAction().FindAllProducts());
            Console.WriteLine("\nEnter product id you want to chage.");
            string productId = Console.ReadLine();
            Console.WriteLine("\nChoose parameter you want change.");
            Console.WriteLine("\n1. Id.");
            Console.WriteLine("\n2. Description.");
            Console.WriteLine("\n3. Cost.");
            Console.WriteLine("\nDefault. Exit.");
            string command = Console.ReadLine();
            switch (command) 
            {
                case "1":
                    order = null;
                    ChangeProductIdCompiler(productId);
                    break;
                case "2":
                    ChangeProductDescriptionCompiler(productId);
                    order = null;
                    break;
                case "3":
                    ChangeProductCostCompiler(productId);
                    order = null;
                    break;
                default:
                    break;
            }

        }
        private void ChangeProductIdCompiler(string productId)
        {
            Console.WriteLine("\nEnter new id.");
            string newId = Console.ReadLine();
            try
            {
                Console.WriteLine(actionFactory.GetAdminAction().ChangeProductId(productId, newId));
            }
            catch (ArgumentNullException ex)
            {
                Console.WriteLine(ex.Message);
            }
            catch (ArgumentException ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
        private void ChangeProductDescriptionCompiler(string productId)
        {
            Console.WriteLine("\nEnter new description.");
            string description = Console.ReadLine();
            try
            {
                Console.WriteLine(actionFactory.GetAdminAction().ChangeProductDescription(productId, description));
            }
            catch (ArgumentNullException ex)
            {
                Console.WriteLine(ex.Message);
            }
            catch (ArgumentException ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
        private void ChangeProductCostCompiler(string productId)
        {
            Console.WriteLine("\nEnter new cost.");
            Console.WriteLine("\nInput format: XXX.XX");
            string cost = Console.ReadLine();
            try
            {
                Console.WriteLine(actionFactory.GetAdminAction().ChangeProductCost(productId, cost));
            }
            catch (ArgumentNullException ex)
            {
                Console.WriteLine(ex.Message);
            }
            catch (ArgumentException ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
        private void ChangeOrderStatusCompiler()
        {
            Console.WriteLine(actionFactory.GetAdminAction().GetAllOrders());
            Console.WriteLine("\nEnter order id which status you want to chage.");
            string orderId = Console.ReadLine();
            Console.WriteLine("\nChoose status to set.");
            Console.WriteLine("\n1. New.");
            Console.WriteLine("\n2. CanceledByAdmin.");
            Console.WriteLine("\n3. PaymentReceived.");
            Console.WriteLine("\n4. Sent.");
            Console.WriteLine("\n5. Received.");
            Console.WriteLine("\n6. Completed.");
            Console.WriteLine("\nDefault. Exit.");
            string command = Console.ReadLine();
            try
            {
                switch (command)
                {
                    case "1":
                        Console.WriteLine(actionFactory.GetAdminAction().SetOrderStatus(orderId, OrderStatus.New));
                        break;
                    case "2":
                        Console.WriteLine(actionFactory.GetAdminAction().SetOrderStatus(orderId, OrderStatus.CanceledByAdmin));
                        break;
                    case "3":
                        Console.WriteLine(actionFactory.GetAdminAction().SetOrderStatus(orderId, OrderStatus.PaymentReceived));
                        break;
                    case "4":
                        Console.WriteLine(actionFactory.GetAdminAction().SetOrderStatus(orderId, OrderStatus.Sent));
                        break;
                    case "5":
                        Console.WriteLine(actionFactory.GetAdminAction().SetOrderStatus(orderId, OrderStatus.Received));
                        break;
                    case "6":
                        Console.WriteLine(actionFactory.GetAdminAction().SetOrderStatus(orderId, OrderStatus.Completed));
                        break;
                    default:
                        break;
                }
                
            }
            catch(ArgumentNullException ex)
            {
                Console.WriteLine(ex.Message);
            }
            catch(ArgumentException ex)
            {
                Console.WriteLine(ex.Message);
            }
            
        }

    }
}
