﻿using System;
using System.Collections.Generic;
using System.Text;

namespace UIL.Interraction
{
    public class InterractionFactory
    {
        public GuestInterraction GetGuestInterraction()
        {
            return new GuestInterraction();
        }
        public UserInterraction GetUserInterraction()
        {
            return new UserInterraction();
        }
        public AdminInterraction GetAdminInterraction()
        {
            return new AdminInterraction();
        }
    }
}
