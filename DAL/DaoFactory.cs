﻿using System;
using System.Collections.Generic;
using System.Text;
using DAL.Dao;
using DAL.IDao;

namespace DAL
{
    public class DaoFactory
    {
        public IUserDao GetUserDao() { return new UserDao(); }
        public IProductDao GetProductDao() { return new ProductDao(); }
        public IOrderDao GetOrderDao() { return new OrderDao(); } 
    }
}
