﻿using System;
using System.Collections.Generic;
using System.Text;
using NUnit.Framework;
using ConsoleEShopMultilayered;
using BAL.Action;

namespace BAL.Tests
{
    [TestFixture]
    class GuestActionTests
    {
        [Test]
        public void FindProductByNameThrowsArgumentNullException()
        {
            GuestAction guestAction = new GuestAction();
            Assert.Throws<ArgumentNullException>(() => guestAction.FindProductByName(""));
        }
        [TestCase("", "Password")]
        [TestCase("login", "")]
        public void LogInThrowsArgumentNullException(string nickname, string password)
        {
            GuestAction guestAction = new GuestAction();
            Assert.Throws<ArgumentNullException>(() => guestAction.LogIn(nickname, password));
        }
        [TestCase("", "Password")]
        [TestCase("login", "")]
        public void SignInAsUserThrowsArgumentNullException(string nickname, string password)
        {
            GuestAction guestAction = new GuestAction();
            Assert.Throws<ArgumentNullException>(() => guestAction.SignInAsUser(nickname, password));
        }
        [TestCase("", "Password")]
        [TestCase("login", "")]
        public void SignInAsAdminThrowsArgumentNullException(string nickname, string password)
        {
            GuestAction guestAction = new GuestAction();
            Assert.Throws<ArgumentNullException>(() => guestAction.SignInAsAdmin(nickname, password));
        }
    }
}
