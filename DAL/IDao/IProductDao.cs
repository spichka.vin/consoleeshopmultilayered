﻿using System;
using System.Collections.Generic;
using System.Text;
using DB;

namespace DAL.IDao
{
    public interface IProductDao
    {
        List<Product> GetAll();
        List<Product> GetProductByName(string name);
        List<Product> GetProductById(int id);
        void Add(Product product);
        void SetId(Product product, int id);
        void SetDescription(Product product, string description);
        void SetCost(Product product, decimal cost);
    }
}
