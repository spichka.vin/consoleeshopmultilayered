﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using DB;
using DB.Users;
using DAL;

namespace BAL.Action
{
    public class GuestAction
    {
        private readonly DaoFactory daoFactory = new DaoFactory();
        public string FindAllProducts()
        {
            string products = "";
            foreach(Product product in daoFactory.GetProductDao().GetAll())
            {
                products += product.ToString();
            }
            return products;
        }
        public string FindProductByName(String name)
        {
            if(name.Length == 0)
            {
                throw new ArgumentNullException(nameof(name), "Argument is null.");
            }
            string str = "";
            foreach(Product product in daoFactory.GetProductDao().GetProductByName(name))
            {
                str += product.ToString();
            }
            if(str.Length == 0)
            {
                return ("\nThere is no products with such name.");
            }
            return str;
        }
        public Guest LogIn(string nickname, string password)
        {
            if (nickname.Length == 0)
            {
                throw new ArgumentNullException(nameof(nickname), "Argument is null.");
            }
            if(password.Length == 0)
            {
                throw new ArgumentNullException(nameof(password), "Argument is null.");
            }
            List<Guest> guest = daoFactory.GetUserDao().GetUserByNickname(nickname);
            if(guest.Count == 0 || !guest.First().Password.Equals(password))
            {
                return null;
            }
            return guest.First();
        }
        public bool SignInAsUser(string nickname, string password)
        {
            if(nickname.Length == 0)
            {
                throw new ArgumentNullException(nameof(nickname), "Argument is null.");
            }
            if(password.Length == 0)
            {
                throw new ArgumentNullException(nameof(password), "Argument is null.");
            }
            List<Guest> guest = daoFactory.GetUserDao().GetUserByNickname(nickname);
            if (guest.Count != 0)
            {
                return false;
            }
            int id = daoFactory.GetUserDao().GetAll().Count + 1;
            User newUser = new User(nickname, password);
            daoFactory.GetUserDao().Add(newUser);
            return true;
        }
        public bool SignInAsAdmin(string nickname, string password)
        {
            if (nickname.Length == 0)
            {
                throw new ArgumentNullException(nameof(nickname), "Argument is null.");
            }
            if (password.Length == 0)
            {
                throw new ArgumentNullException(nameof(password), "Argument is null.");
            }
            List<Guest> guest = daoFactory.GetUserDao().GetUserByNickname(nickname);
            if (guest.Count != 0)
            {
                return false;
            }
            int id = daoFactory.GetUserDao().GetAll().Count + 1;
            Admin newUser = new Admin(nickname, password);
            daoFactory.GetUserDao().Add(newUser);
            return true;
        }
    }
}
