﻿using System;
using System.Collections.Generic;
using NUnit.Framework;
using System.Linq;
using ConsoleEShopMultilayered;
using DAL.IDao;
using DAL.Dao;
using DB;


namespace DAL.Tests
{
    [TestFixture]
    class ProductDaoTests
    {
        [SetUp]
        public void Init()
        {
            DataInitializer initializer = new DataInitializer();
            initializer.ProductInitialize();
        }
        [TearDown]
        public void Declin()
        {
            DataInitializer initializer = new DataInitializer();
            initializer.Decliner();
        }
        [TestCase(3)]
        public void GetAllReturnsRightProductCount(int expected)
        {
            IProductDao productDao = new ProductDao();
            Assert.AreEqual(expected, productDao.GetAll().Count);
        }
        [TestCase("Asus ROG")]
        public void GetProductByNameReturnsRightProductList(string name)
        {
            IProductDao productDao = new ProductDao();
            List<Product> productList = productDao.GetProductByName(name);
            Assert.AreEqual(name, productList.First().Name);
        }
        [Test]
        public void GetProductByIdReturnsRightProductList()
        {
            Product product = new Product("XBox", "Game consol", "", 400);
            IProductDao productDao = new ProductDao();
            int count = productDao.GetAll().Count;
            productDao.Add(product);
            Assert.AreEqual(count + 1, productDao.GetAll().Count);
        }
        [TestCase(1, "Asus ROG")]
        public void AddIncreasProductCount(int id, string name)
        {
            IProductDao productDao = new ProductDao();
            List<Product> userList = productDao.GetProductById(id);
            Assert.AreEqual(name, userList.First().Name);
        }
        [TestCase("Asus ROG", 10)]
        public void SetIdChangesProductId(string name, int id)
        {
            IProductDao productDao = new ProductDao();
            Product product = productDao.GetProductByName(name).First();
            productDao.SetId(product, id);
            Assert.AreEqual(productDao.GetProductByName(name).First(), productDao.GetProductById(id).First());
        }
        [TestCase("Asus ROG", 10)]
        public void SetCostChangesProductCost(string name, int cost)
        {
            IProductDao productDao = new ProductDao();
            Product product = productDao.GetProductByName(name).First();
            productDao.SetCost(product, cost);
            Assert.AreEqual(cost, productDao.GetProductByName(name).First().Cost);
        }
    }
}
