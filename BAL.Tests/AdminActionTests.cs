﻿using System;
using System.Linq;
using NUnit.Framework;
using ConsoleEShopMultilayered;
using BAL.Action;
using DB.Users;
using DB;

namespace BAL.Tests
{
    [TestFixture]
    class AdminActionTests
    {
        [SetUp]
        public void Init()
        {
            DataInitializer initializer = new DataInitializer();
            initializer.UserInitializer();
            initializer.ProductInitialize();
            initializer.OrderInitializer();
        }
        [TearDown]
        public void Declin()
        {
            DataInitializer initializer = new DataInitializer();
            initializer.Decliner();
        }
        [TestCase("", "1")]
        [TestCase("1", "")]
        public void ChangeUserIdThrowsArgumentNullException(string oldId, string newId)
        {
            AdminAction adminAction = new AdminAction();
            Assert.Throws<ArgumentNullException>(() => adminAction.ChangeUserId(oldId, newId));
        }
        [TestCase("asd", "1")]
        [TestCase("1", "asd")]
        public void ChangeUserIdThrowsArgumentException(string oldId, string newId)
        {
            AdminAction adminAction = new AdminAction();
            Assert.Throws<ArgumentException>(() => adminAction.ChangeUserId(oldId, newId));
        }
        [TestCase("")]
        public void GetUserByIdThrowsArgumentNullException(string id)
        {
            AdminAction adminAction = new AdminAction();
            Assert.Throws<ArgumentNullException>(() => adminAction.GetUserById(id));
        }
        [TestCase("asd")]
        public void GetUserByIdThrowsArgumentException(string id)
        {
            AdminAction adminAction = new AdminAction();
            Assert.Throws<ArgumentException>(() => adminAction.GetUserById(id));
        }
        [TestCase("")]
        public void GetProductByIdThrowsArgumentNullException(string id)
        {
            AdminAction adminAction = new AdminAction();
            Assert.Throws<ArgumentNullException>(() => adminAction.GetProductById(id));
        }
        [TestCase("asd")]
        public void GetProductByIdThrowsArgumentException(string id)
        {
            AdminAction adminAction = new AdminAction();
            Assert.Throws<ArgumentException>(() => adminAction.GetProductById(id));
        }
        [TestCase("", "category", "description", "22.22")]
        [TestCase("name", "", "description", "22.22")]
        [TestCase("name", "category", "description", "")]
        public void CreateProductThrowsArgumentNullException(string name, string category, string description, string cost)
        {
            AdminAction adminAction = new AdminAction();
            Assert.Throws<ArgumentNullException>(() => adminAction.CreateProduct(name, category, description, cost));
        }
        [TestCase("name", "category", "description", "211as")]
        public void CreateProductThrowsArgumentException(string name, string category, string description, string cost)
        {
            AdminAction adminAction = new AdminAction();
            Assert.Throws<ArgumentException>(() => adminAction.CreateProduct(name, category, description, cost));
        }
        [TestCase("", "1")]
        [TestCase("1", "")]
        public void ChangeProductIdThrowsArgumentNullException(string oldId, string newId)
        {
            AdminAction adminAction = new AdminAction();
            Assert.Throws<ArgumentNullException>(() => adminAction.ChangeProductId(oldId, newId));
        }
        [TestCase("asd", "1")]
        [TestCase("1", "asd")]
        public void ChangeProductIdThrowsArgumentException(string oldId, string newId)
        {
            AdminAction adminAction = new AdminAction();
            Assert.Throws<ArgumentException>(() => adminAction.ChangeProductId(oldId, newId));
        }
        [TestCase("", "1")]
        [TestCase("1", "")]
        public void ChangeProductCostThrowsArgumentNullException(string oldId, string cost)
        {
            AdminAction adminAction = new AdminAction();
            Assert.Throws<ArgumentNullException>(() => adminAction.ChangeProductCost(oldId, cost));
        }
        [TestCase("asd", "1")]
        [TestCase("1", "asd")]
        public void ChangeProductCostThrowsArgumentException(string oldId, string cost)
        {
            AdminAction adminAction = new AdminAction();
            Assert.Throws<ArgumentException>(() => adminAction.ChangeProductCost(oldId, cost));
        }
        [TestCase("")]
        public void ChangeProductDescriptionThrowsArgumentNullException(string id)
        {
            AdminAction adminAction = new AdminAction();
            Assert.Throws<ArgumentNullException>(() => adminAction.ChangeProductDescription(id, "description"));
        }
        [TestCase("asd")]
        public void ChangeProductDescriptionThrowsArgumentException(string id)
        {
            AdminAction adminAction = new AdminAction();
            Assert.Throws<ArgumentException>(() => adminAction.ChangeProductDescription(id, "description"));
        }
        [TestCase("")]
        public void SetOrderStatusThrowsArgumentNullException(string id)
        {
            AdminAction adminAction = new AdminAction();
            Assert.Throws<ArgumentNullException>(() => adminAction.SetOrderStatus(id, OrderStatus.Received));
        }
        [TestCase("asd")]
        public void SetOrderStatusThrowsArgumentException(string id)
        {
            AdminAction adminAction = new AdminAction();
            Assert.Throws<ArgumentException>(() => adminAction.SetOrderStatus(id, OrderStatus.Received));
        }
    }
}
