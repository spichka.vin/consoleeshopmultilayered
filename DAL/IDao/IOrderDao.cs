﻿using System;
using System.Collections.Generic;
using System.Text;
using DB;
using DB.Users;

namespace DAL.IDao
{
    public interface IOrderDao
    {
        List<Order> GetAll();
        void CreateOrder(Order order);
        List<Order> GetAllByUser(Guest user);
        List<Order> GetAllByUserAndOrderStatus(Guest user, OrderStatus status);
        List<Order> GetOrderById(int id);
        void SetOrderStatus(Order order, OrderStatus status);
        void UpdateUserInAllOrder(Guest oldUser, Guest newUser);
        void UpdateProductInAllOrder(Product oldProduct, Product newProduct);
    }
}
