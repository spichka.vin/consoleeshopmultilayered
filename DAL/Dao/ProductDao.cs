﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using DB;
using DAL.IDao;

namespace DAL.Dao
{
    public class ProductDao : IProductDao
    {
        public ProductDao() { }
        public List<Product> GetAll()
        {
            return Data.productList;
        }
        public List<Product> GetProductByName(string name)
        {
            return Data.productList.Where(x => x.Name.Equals(name)).ToList();
        }
        public List<Product> GetProductById(int id)
        {
            return Data.productList.Where(x => x.Id.Equals(id)).ToList();
        }
        public void Add(Product product)
        {
            Data.productList.Add(product);
        }
        public void SetId(Product product, int id)
        {
            Data.productList.Where(x => x.Equals(product)).First().Id = id;
        }
        public void SetDescription(Product product, string description)
        {
            if(description.Length == 0)
            {
                description = "No information.";
            }
            Data.productList.Where(x => x.Equals(product)).First().Description = description;
        }
        public void SetCost(Product product, decimal cost)
        {
            Data.productList.Where(x => x.Equals(product)).First().Cost = cost;
        }
    }
}
