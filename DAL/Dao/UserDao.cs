﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using DB;
using DB.Users;
using DAL.IDao;

namespace DAL.Dao
{
    public class UserDao : IUserDao
    {
        public UserDao() { }
        public List<Guest> GetAll()
        {
            return Data.userList.ToList();
        }
        public List<Guest> GetUserByNickname(string nickname)
        {
            return Data.userList.Where(x => x.Nickname.Equals(nickname)).ToList();
        }
        public List<Guest> GetUserById(int id)
        {
            return Data.userList.Where(x => x.Id == id).ToList();
        }
        public void Add(Guest user)
        {
            Data.userList.Add(user);
        }
        public void SetId(Guest user, int id)
        {
            Data.userList.Where(x => x.Equals(user)).First().Id = id;
        }
        public void SetPassword(Guest user, string password)
        {
            Data.userList.Where(x => x.Equals(user)).First().Password = password;
        }
        public void SetAge(Guest user, int age)
        {
            Data.userList.Where(x => x.Equals(user)).First().Age = age;
        }
    }
}
