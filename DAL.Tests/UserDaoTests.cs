﻿using NUnit.Framework;
using System.Linq;
using ConsoleEShopMultilayered;
using DAL.IDao;
using DAL.Dao;
using DB.Users;
using System.Collections.Generic;

namespace DAL.Tests
{
    [TestFixture]
    class UserDaoTests
    {
        [SetUp]
        public void Init()
        {
            DataInitializer initializer = new DataInitializer();
            initializer.UserInitializer();
        }
        [TearDown]
        public void Declin()
        {
            DataInitializer initializer = new DataInitializer();
            initializer.Decliner();
        }
        [TestCase(3)]
        public void GetAllReturnsRightUserCount(int expected)
        {
            IUserDao userDao = new UserDao();
            Assert.AreEqual(expected, userDao.GetAll().Count);
        }
        [TestCase("user")]
        [TestCase("Alevs")]
        public void GetUserByNicknameReturnsRightUserList(string nickname)
        {
            IUserDao userDao = new UserDao();
            List<Guest> userList = userDao.GetUserByNickname(nickname);
            Assert.AreEqual(nickname, userList.First().Nickname);
        }
        [TestCase("user1")]
        public void GetUserByNicknameReturnsEmptyUserList(string nickname)
        {
            IUserDao userDao = new UserDao();
            List<Guest> userList = userDao.GetUserByNickname(nickname);
            Assert.AreEqual(0, userList.Count);
        }
        [TestCase(1, "admin")]
        public void GetUserByIdReturnsRightUserList(int id, string nickname)
        {
            IUserDao userDao = new UserDao();
            List<Guest> userList = userDao.GetUserById(id);
            Assert.AreEqual(nickname, userList.First().Nickname);
        }
        [TestCase(7)]
        public void GetUserByIdReturnsEmptyUserList(int id)
        {
            IUserDao userDao = new UserDao();
            List<Guest> userList = userDao.GetUserById(id);
            Assert.AreEqual(0, userList.Count);
        }
        [Test]
        public void AddIncreasUserCount()
        {
            Guest user = new User("monster", "1234");
            IUserDao userDao = new UserDao();
            int count = userDao.GetAll().Count;
            userDao.Add(user);
            Assert.AreEqual(count + 1, userDao.GetAll().Count);
        }
        [TestCase("user" , 10)]
        public void SetIdChangesUserId(string nickname, int id)
        {
            IUserDao userDao = new UserDao();
            Guest user = userDao.GetUserByNickname(nickname).First();
            userDao.SetId(user, id);
            Assert.AreEqual(userDao.GetUserByNickname(nickname).First(), userDao.GetUserById(id).First());
        }
        [TestCase("mypassword", 1)]
        public void SetPasswordChangesUserPassword(string password, int id)
        {
            IUserDao userDao = new UserDao();
            Guest user = userDao.GetUserById(id).First();
            userDao.SetPassword(user, password);
            Assert.AreEqual(password, userDao.GetUserById(id).First().Password);
        }
        [TestCase(60, 2)]
        public void SetAgeChangesUserPassword(int age, int id)
        {
            IUserDao userDao = new UserDao();
            Guest user = userDao.GetUserById(id).First();
            userDao.SetAge(user, age);
            Assert.AreEqual(age, userDao.GetUserById(id).First().Age);
        }
    }   
}
