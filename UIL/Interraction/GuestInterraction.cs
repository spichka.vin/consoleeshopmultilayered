﻿using System;
using System.Collections.Generic;
using System.Text;
using DB.Users;

namespace UIL.Interraction
{
    public class GuestInterraction : BaseInterraction
    {
        
        public void ShowMenu(Guest user)
        {
            base.ShowMenu(user);
            Console.WriteLine("3. Sign in as a new customer.");
            Console.WriteLine("4. Sign in as a new admin.");
            Console.WriteLine("5. Log in to your account.");
            ExitMenu();
        }
        public bool RequestCompiler(string command, ref Guest user)
        {
            switch (command) 
            {
                case "3":
                    SignInAsUser();
                    break;
                case "4":
                    SignInAsAdmin();
                    break;
                case "5":
                    LogInCompiler(ref user);
                    break;
                default:
                    return base.BaseRequestCompiler(command);
            }
            return true;
        }

        private void LogInCompiler(ref Guest user)
        {
            Console.WriteLine("\nInput nickname:");
            string nickname = Console.ReadLine();
            Console.WriteLine("Input password:");
            string password = Console.ReadLine();
            try
            {             
                Guest newUser = actionFactory.GetGuestAction().LogIn(nickname, password);
                if(newUser is null)
                {
                    Console.WriteLine("\nIncorrect nickname or password.");
                    return;
                }
                 user = newUser;
            }
            catch(ArgumentNullException ex)
            {
                Console.WriteLine("\n" + ex.Message);
            }
        }

        private void SignInAsUser()
        {
            Console.WriteLine("\nInput nickname:");
            string nickname = Console.ReadLine();
            Console.WriteLine("Input password:");
            string password = Console.ReadLine();
            try
            {
                bool flag = actionFactory.GetGuestAction().SignInAsUser(nickname, password);
                if (flag)
                {
                    Console.WriteLine("\nYou have registered seccessfuly.");
                }
                else
                {
                    Console.WriteLine("\nUser with such nickname already exist.");
                }
            }
            catch (ArgumentNullException ex)
            {
                Console.WriteLine("\n" + ex.Message);
            }
        }

        private void SignInAsAdmin()
        {
            Console.WriteLine("\nInput nickname:");
            string nickname = Console.ReadLine();
            Console.WriteLine("Input password:");
            string password = Console.ReadLine();
            try
            {
                bool flag = actionFactory.GetGuestAction().SignInAsAdmin(nickname, password);
                if (flag)
                {
                    Console.WriteLine("\nYou have registered seccessfuly.");
                }
                else
                {
                    Console.WriteLine("\nUser with such nickname already exist.");
                }
            }
            catch (ArgumentNullException ex)
            {
                Console.WriteLine("\n" + ex.Message);
            }
        }

    }
}
