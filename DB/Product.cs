﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace DB
{
    public class Product
    {
        public int Id
        {
            get; set;
        }
        public string Name
        {
            get; set;
        }
        public string Category
        {
            get; set;
        }
        public string Description
        {
            get; set;
        }
        public decimal Cost
        {
            get; set;
        }
        public Product(string name, string category, string description, decimal cost)
        {
            if(Data.productList.Count == 0)
            {
                this.Id = 1;
            }
            else
            {
                this.Id = Data.productList.Select(x => x.Id).Max() + 1;
            }
            this.Name = name;
            this.Category = category;
            if(description == "")
            {
                this.Description = "No information.";
            }
            else
            {
                this.Description = description;
            }            
            this.Cost = cost;
        }
        public override string ToString()
        {

            string str = "\nId: " + this.Id.ToString() + "\n";
            str += "Name: " + this.Name + "\n";
            str += "Category: " + this.Category + "\n";
            str += "Descriptioin: " + this.Description + "\n";
            str += "Cost: " + Convert.ToString(this.Cost) + "\n";
            return str;
        }
    }
}
