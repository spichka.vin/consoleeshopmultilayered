﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using DB;
using DAL.IDao;
using DB.Users;

namespace DAL.Dao
{
    public class OrderDao : IOrderDao
    {
        public OrderDao() { }
        public List<Order> GetAll()
        {
            return Data.orderList;
        }
        public List<Order> GetAllByUser(Guest user)
        {
            return Data.orderList.Where(x => x.Customer.Nickname.Equals(user.Nickname)).ToList();
        }
        public void CreateOrder(Order order)
        {
            Data.orderList.Add(order);
        }
        public List<Order> GetAllByUserAndOrderStatus(Guest user, OrderStatus status)
        {
            return Data.orderList.Where(x => x.Customer.Equals(user) && 
            x.Status.Equals(status)).ToList();
        }
        public List<Order> GetOrderById(int id)
        {
            return Data.orderList.Where(x => x.Id == id).ToList();
        }
        public void SetOrderStatus(Order order, OrderStatus status)
        {
            Order orderForChange = GetOrderById(order.Id).First();
            orderForChange.Status = status;
        }
        public void UpdateUserInAllOrder(Guest oldUser, Guest newUser)
        {
            foreach(Order order in Data.orderList)
            {
                if (order.Customer.Equals(oldUser))
                {
                    order.Customer = newUser;
                }
            }
        }
        public void UpdateProductInAllOrder(Product oldProduct, Product newProduct)
        {
            foreach (Order order in Data.orderList)
            {
                if (order.Product.Equals(oldProduct))
                {
                    order.Product = newProduct;
                }
            }
        }
    }
}
