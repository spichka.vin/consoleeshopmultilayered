﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using DB.Users;
using DB;
using DAL;

namespace BAL.Action
{
    public class UserAction
    {
        private readonly DaoFactory daoFactory = new DaoFactory();
        public Order CreateNewOrder(Guest user, string orderId)
        {
            if(orderId.Length == 0)
            {
                throw new ArgumentNullException(nameof(orderId), "Argument is null.");
            }
            if (!IsNumber(orderId))
            {
                throw new ArgumentException("Value is not number.", nameof(orderId));
            }
            List<Product> priductList = daoFactory.GetProductDao().GetProductById(Convert.ToInt32(orderId));
            if(priductList.Count == 0)
            {
                return null;
            }
            return new Order(user, priductList.First());
        }
        public void ConfirmOrderCreating(Order order)
        {
            Data.orderList.Add(order);
        }
        public string FindOrderHistory(Guest user)
        {
            List<Order> orderList = daoFactory.GetOrderDao().GetAllByUser(user);
            string msg = "\nYour order history is empty.";
            return CreateString(orderList, msg);
        }
        public string GetAllOrderWithSendStatus(Guest user)
        {
            List<Order> orderList = daoFactory.GetOrderDao().GetAllByUserAndOrderStatus(user, OrderStatus.Sent);
            string msg = "\nYou don`t have sended orders.";
            return CreateString(orderList, msg);
        }
        public string GetOrderWithSentPaymentReceivedNewStatus(Guest user)
        {
            List<Order> orderListSent = daoFactory.GetOrderDao().GetAllByUserAndOrderStatus(user, OrderStatus.Sent);
            List<Order> orderListPaymentReceived = daoFactory.GetOrderDao().GetAllByUserAndOrderStatus(user, OrderStatus.PaymentReceived);
            List<Order> orderListNew = daoFactory.GetOrderDao().GetAllByUserAndOrderStatus(user, OrderStatus.New);
            foreach(Order order in orderListSent)
            {
                orderListNew.Add(order);
            }
            foreach(Order order in orderListPaymentReceived)
            {
                orderListNew.Add(order);
            }
            string msg = "\nThere is no order you can change status.";
            return CreateString(orderListNew, msg);
        }
        public string SetOrderStatus(string orderId, Guest user, OrderStatus status)
        {
            if (orderId.Length == 0)
            {
                throw new ArgumentNullException(nameof(orderId), "Argument is null.");
            }
            if (!IsNumber(orderId))
            {
                throw new ArgumentException("Value is not a number.", nameof(orderId));
            }
            List<Order> order = daoFactory.GetOrderDao().GetOrderById(Convert.ToInt32(orderId));
            if(order.Count == 0 ||
                order.First().Status != OrderStatus.Sent ||
                !order.First().Customer.Equals(user))
            {
                return "\nIncorrect order id.";
            }
            if (order.First().Status.Equals(OrderStatus.Received))
            {
                return "\nOrder already received.";
            }
            if (order.First().Status.Equals(OrderStatus.Canceled) ||
                order.First().Status.Equals(OrderStatus.CanceledByAdmin))
            {
                return "\nOrder already canceled.";
            }
            daoFactory.GetOrderDao().SetOrderStatus(order.First(), status);
            return "\nOrder status was changed.";
        }
        public string SetUserPassword(Guest user, string password)
        {
            if(password.Length == 0)
            {
                throw new ArgumentNullException(nameof(password), "Argument is null.");
            }
            daoFactory.GetUserDao().SetPassword(user, password);
            OrdersUpdate(user);
            return "\nPassword was changeg.";
        }
        public string SetUserAge(Guest user, string age)
        {
            if (age.Length == 0)
            {
                throw new ArgumentNullException(nameof(age), "Argument is null.");
            }
            if (!IsNumber(age) || 
                Convert.ToInt32(age) < 14 || 
                Convert.ToInt32(age) > 100)
            {
                throw new ArgumentException("Incorrect value.", nameof(age));
            }
            daoFactory.GetUserDao().SetAge(user, Convert.ToInt32(age));
            OrdersUpdate(user);
            return "\nAge was canged.";
        }
        private void OrdersUpdate(Guest user)
        {
            Guest newUser = daoFactory.GetUserDao().GetUserById(user.Id).First();
            daoFactory.GetOrderDao().UpdateUserInAllOrder(user, newUser);
        }
        private bool IsNumber(string str)
        {
            string newStr = new String(str.Where(c => c != '-' && (c < '0' || c > '9')).ToArray());
            if (newStr.Length > 0)
            {
                return false;
            }
            return true;
        }
        private string CreateString(List<Order> orderList, string msg)
        {
            string str = "";
            foreach (Order order in orderList)
            {
                str += order.ToString();
            }
            if (str.Length == 0)
            {
                return msg;
            }
            return str;
        }
    }
}
